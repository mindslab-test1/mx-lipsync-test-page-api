package maum.ai.lipsync_demo_spring.client

import com.google.protobuf.ByteString
import io.grpc.ManagedChannel
import io.grpc.ManagedChannelBuilder
import io.grpc.stub.StreamObserver
import maum.brain.wav2lip.BrainWav2Lip
import maum.brain.wav2lip.Wav2LipGenerationGrpc
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import org.springframework.web.multipart.MultipartFile
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit

@Component
class Wav2LipClient {
    private val channelMap: MutableMap<String, ChannelHolder> = HashMap()
    private val logger = LoggerFactory.getLogger(Wav2LipClient::class.java)

    fun determinate(
        audio: MultipartFile,
        backgroundImg: MultipartFile? = null,
        resolution: String,
        wav2LipIp: String,
        wav2LipPort: Int
    ): ByteArrayOutputStream {
        logger.info("wav2lip grpc server address ::: $wav2LipIp:${wav2LipPort}")
        val wav2LipChannel = "$wav2LipIp:$wav2LipPort"
        val channel: ManagedChannel
        if(channelMap.containsKey(wav2LipChannel)) {
            channel = channelMap[wav2LipChannel]!!.channel
            channelMap[wav2LipChannel]!!.lastReferencedEpoch = System.currentTimeMillis()
        } else {
            channel = getManagedChannelAddress(wav2LipIp, wav2LipPort)!!
            channelMap[wav2LipChannel] = ChannelHolder(channel, System.currentTimeMillis())
        }
        val stub: Wav2LipGenerationGrpc.Wav2LipGenerationStub = Wav2LipGenerationGrpc.newStub(channel)

        val responseList: MutableList<BrainWav2Lip.Wav2LipResult> = ArrayList()
        val errorList: MutableList<Throwable> = ArrayList()
        val finishLatch = CountDownLatch(1)

        val responseObserver: StreamObserver<BrainWav2Lip.Wav2LipResult> = object : StreamObserver<BrainWav2Lip.Wav2LipResult> {


            override fun onNext(wav2LipResult: BrainWav2Lip.Wav2LipResult) {
                logger.info("RESPONSE OBSERVER NEXT")
                logger.info("WAV2LIP RESULT ::: ${wav2LipResult.video.size()}")
                responseList.add(wav2LipResult)
            }

            override fun onError(throwable: Throwable) {
                logger.info("RESPONSE OBSERVER ERROR ")
                logger.error(throwable.message)
                errorList.add(throwable)
                finishLatch.countDown()
            }

            override fun onCompleted() {
                logger.info("RESPONSE OBSERVER COMPLETE")
                logger.debug("server on completed called")
                finishLatch.countDown()
            }

        }
        val requestObserver = stub.determinate(responseObserver)

        val inputBuilder = BrainWav2Lip.Wav2LipInput.newBuilder()
        requestObserver.onNext(
            inputBuilder
                .setResolution(this.getResolutionEnum(resolution))
                .setTransparent(backgroundImg == null)
                .build()
        )

        val audioBytes = audio.bytes
        val byteArrayInputStreamAudio = ByteArrayInputStream(audioBytes)
        var lengthAudio :Int
        var bufferAudio = ByteArray(1024 * 512)
        while(byteArrayInputStreamAudio.read(bufferAudio).also { lengthAudio = it } > 0) {
            logger.debug(String.format("image chunk size: %d", lengthAudio))
            if (lengthAudio < bufferAudio.size) bufferAudio = bufferAudio.copyOfRange(0, lengthAudio)
            val inputByteString = ByteString.copyFrom(bufferAudio)
            requestObserver.onNext(inputBuilder.setAudio(inputByteString).build())
        }


        logger.info("RESPONSE OBSERVER")
        for (audioByte in audioBytes) run {
            val inputByteString: ByteString = ByteString.copyFrom(byteArrayOf(audioByte))
            requestObserver.onNext(inputBuilder.setAudio(inputByteString).build())
        }
        backgroundImg?.let {
            val byteArrayInputStreamImg = ByteArrayInputStream(backgroundImg.bytes)
            var lengthImg: Int
            var bufferImg = ByteArray(1024 * 512)

            while (byteArrayInputStreamImg.read(bufferImg).also { lengthImg = it } > 0) {
                logger.debug(String.format("image chunk size: %d", lengthImg))
                if (lengthImg < bufferImg.size) bufferImg = bufferImg.copyOfRange(0, lengthImg)
                val inputByteString = ByteString.copyFrom(bufferImg)
                requestObserver.onNext(inputBuilder.setBackground(inputByteString).build())
                logger.debug("send image chunk to server")
            }
        }

        logger.info("input builder ::: Transparent ${inputBuilder.transparent}")

        requestObserver.onCompleted()

        if (!finishLatch.await(3, TimeUnit.MINUTES)) {
            logger.error("connection timeout")
        }
        if (errorList.isNotEmpty()) {
            logger.error(errorList[0].toString())
        }
        val outputStream = ByteArrayOutputStream()
        if (responseList.size>0) {
            outputStream.write(responseList[0].video.toByteArray())
        }
        logger.info("RESULT DATA SIZE ::: ${outputStream.size()}")
        return outputStream
    }

    fun getManagedChannelAddress(ip: String?, port: Int): ManagedChannel? {
        return ManagedChannelBuilder.forAddress(ip, port)
            .usePlaintext()
            .keepAliveWithoutCalls(true)
            .build()
    }
    private class ChannelHolder constructor(var channel: ManagedChannel, var lastReferencedEpoch: Long)

    private fun getResolutionEnum(resolution: String): BrainWav2Lip.VideoResolution {
        return when (resolution) {
            "SD" -> BrainWav2Lip.VideoResolution.RES_LOW
            "HD" -> BrainWav2Lip.VideoResolution.RES_MEDIUM
            "FHD" -> BrainWav2Lip.VideoResolution.RES_HIGH
            else -> throw Exception(
                "unknown resolution type"
            )
        }
    }
}