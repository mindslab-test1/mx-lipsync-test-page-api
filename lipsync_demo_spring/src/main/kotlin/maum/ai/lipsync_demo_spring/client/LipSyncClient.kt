package maum.ai.lipsync_demo_spring.client

import com.google.protobuf.ByteString
import io.grpc.ManagedChannel
import io.grpc.ManagedChannelBuilder
import io.grpc.stub.StreamObserver
import maum.ai.lipsync_demo_spring.util.LipSyncKey
import maum.ai.lipsync_demo_spring.util.LipSyncStatus
import maum.brain.lipsync.BrainLipsync
import maum.brain.lipsync.BrainLipsync.LipSyncInput
import maum.brain.lipsync.BrainLipsync.LipSyncRequestKey
import maum.brain.lipsync.LipSyncGenerationGrpc
import maum.brain.lipsync.LipSyncGenerationGrpc.LipSyncGenerationBlockingStub
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import org.springframework.web.multipart.MultipartFile
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.util.*
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit
import javax.annotation.PreDestroy
import kotlin.collections.ArrayList

@Component
class LipSyncClient {


    private val channelMap: MutableMap<String, ChannelHolder> = HashMap()
    private val keyModelMap: MutableMap<String, String> = HashMap()

    private val logger = LoggerFactory.getLogger(LipSyncClient::class.java)


    @Throws(IOException::class, InterruptedException::class)
    fun upload(
        text: String,
        backgroundImg: MultipartFile? = null,
        resolution: String,
        lipSyncIp: String,
        lipSyncPort: Int
    ): LipSyncKey {
        logger.info("lipsync grpc server address ::: $lipSyncIp:${lipSyncPort}")
        val lipChannel = "$lipSyncIp:${lipSyncPort}"
        val channel: ManagedChannel
        if (channelMap.containsKey(lipChannel)) {
            channel = channelMap[lipChannel]!!.channel
            channelMap[lipChannel]!!.lastReferencedEpoch = System.currentTimeMillis()
        } else {
            channel = getManagedChannelAddress(lipSyncIp, lipSyncPort)!!
            channelMap[lipChannel] = ChannelHolder(
                channel, System.currentTimeMillis()
            )
        }
        val stub: LipSyncGenerationGrpc.LipSyncGenerationStub = LipSyncGenerationGrpc.newStub(channel)

        val responseList: MutableList<LipSyncKey> = ArrayList()
        val errorList: MutableList<Throwable> = ArrayList()

        val finishLatch = CountDownLatch(1)
        val responseObserver: StreamObserver<LipSyncRequestKey> = object : StreamObserver<LipSyncRequestKey> {
            override fun onNext(lipSyncRequestKey: LipSyncRequestKey) {
                responseList.add(
                    LipSyncKey(lipSyncRequestKey.requestKey)
                )
                logger.debug("request key received")
            }

            override fun onError(throwable: Throwable) {
                logger.error(throwable.message)
                errorList.add(throwable)
                finishLatch.countDown()
            }

            override fun onCompleted() {
                logger.debug("server on completed called")
                finishLatch.countDown()
            }
        }

        val requestObserver = stub.upload(responseObserver)
        val inputBuilder = LipSyncInput.newBuilder()
        requestObserver.onNext(
            inputBuilder
                .setText("$text  .")
                .setResolution(this.getResolutionEnum(resolution))
                .setTransparent(backgroundImg == null)
//                .setBackground(ByteString.copyFrom(backgroundByte))
                .build()
        )
        logger.debug("send text chunk to server")
        backgroundImg?.let {
            val byteArrayInputStream = ByteArrayInputStream(backgroundImg.bytes)
            var length: Int
            var buffer = ByteArray(1024 * 512)
            while (byteArrayInputStream.read(buffer).also { length = it } > 0) {
                logger.debug(String.format("image chunk size: %d", length))
                if (length < buffer.size) buffer = buffer.copyOfRange(0, length)
                val inputByteString = ByteString.copyFrom(buffer)
                requestObserver.onNext(inputBuilder.setBackground(inputByteString).build())
                logger.debug("send image chunk to server")
            }
        }
        requestObserver.onCompleted()

        if (!finishLatch.await(1, TimeUnit.MINUTES)) {
            logger.error("connection timeout")
        }
        if (errorList.isNotEmpty()) {
            logger.error(errorList[0].toString())

        }
        keyModelMap[responseList[0].requestKey!!] = lipChannel

        logger.info("requestKey ::: ${responseList[0].requestKey}")

        return responseList[0]
    }

    fun statusCheck(requestKey: String): LipSyncStatus {
        val channelAddress = keyModelMap[requestKey] ?: throw Exception("request key is invalid")

        val stub: LipSyncGenerationBlockingStub = if (channelMap[channelAddress] != null) {
            LipSyncGenerationGrpc.newBlockingStub(channelMap[channelAddress]!!.channel)
        } else {
            LipSyncGenerationGrpc.newBlockingStub(
                ChannelHolder(
                    (getManagedChannelAddress(
                        channelAddress.split(":".toRegex()).toTypedArray()[0],
                        channelAddress.split(":".toRegex()).toTypedArray()[1].toInt()
                    )!!
                            ), System.currentTimeMillis()
                ).channel
            )
        }

        val statusMessage = stub.statusCheck(
            LipSyncRequestKey.newBuilder()
                .setRequestKey(requestKey)
                .build()
        )

        logger.debug(statusMessage.statusCode.toString())
        logger.debug(statusMessage.msg)
        logger.debug(statusMessage.waiting.toString())

        return LipSyncStatus(
            statusMessage.statusCode.number,
            statusMessage.msg,
            statusMessage.waiting
        )

    }

    fun download(requestKey: String): ByteArrayOutputStream {
        val channelAddress = keyModelMap[requestKey] ?: throw Exception("request key is invalid")


        val stub: LipSyncGenerationBlockingStub = if (channelMap[channelAddress] != null) {
            LipSyncGenerationGrpc.newBlockingStub(channelMap[channelAddress]!!.channel)
        } else {
            LipSyncGenerationGrpc.newBlockingStub(
                getManagedChannelAddress(
                    channelAddress.split(":".toRegex()).toTypedArray()[0],
                    channelAddress.split(":".toRegex()).toTypedArray()[1].toInt()
                )?.let {
                    ChannelHolder(
                        it, System.currentTimeMillis()
                    ).channel
                }
            )
        }
        val statusMsg = stub.statusCheck(
            LipSyncRequestKey.newBuilder()
                .setRequestKey(requestKey)
                .build()
        )
        if (statusMsg.statusCodeValue != 2) {
            logger.warn(statusMsg.statusCode.toString())
            logger.warn(statusMsg.msg)
            throw Exception("Process is not done")
        }

        val rawIterator = stub.download(
            LipSyncRequestKey.newBuilder()
                .setRequestKey(requestKey)
                .build()
        )

        val outputStream = ByteArrayOutputStream()

        try {
            var i = 1
            while (rawIterator.hasNext()) {
                val response = rawIterator.next()
                outputStream.write(response.video.toByteArray())
                i++
            }
        } catch (e: java.lang.Exception) {
            logger.info("Error: Stream response from Avatar downloadAvatar")
            logger.error(e.message)
            throw Exception(e.message)
        }

        return outputStream
    }

    fun getManagedChannelAddress(ip: String?, port: Int): ManagedChannel? {
        return ManagedChannelBuilder.forAddress(ip, port)
            .usePlaintext()
            .keepAliveWithoutCalls(true)
            .build()
    }

    private class ChannelHolder constructor(var channel: ManagedChannel, var lastReferencedEpoch: Long)


    @PreDestroy
    private fun onDestroy() {
        for ((_, value) in channelMap) {
            value.channel.shutdown()
        }
    }

    private fun getResolutionEnum(resolution: String): BrainLipsync.VideoResolution {
        return when (resolution) {
            "SD" -> BrainLipsync.VideoResolution.RES_LOW
            "HD" -> BrainLipsync.VideoResolution.RES_MEDIUM
            "FHD" -> BrainLipsync.VideoResolution.RES_HIGH
            else -> throw Exception(
                "unknown resolution type"
            )
        }
    }
}