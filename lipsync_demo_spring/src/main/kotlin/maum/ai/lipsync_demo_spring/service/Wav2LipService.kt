package maum.ai.lipsync_demo_spring.service

import maum.ai.lipsync_demo_spring.client.Wav2LipClient
import maum.ai.lipsync_demo_spring.dto.Wav2LipDto
import org.slf4j.LoggerFactory
import org.springframework.core.io.FileSystemResource
import org.springframework.stereotype.Service
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.text.SimpleDateFormat
import java.util.*


@Service
class Wav2LipService {
    private val logger = LoggerFactory.getLogger(Wav2LipService::class.java)
    private val grpcClient: Wav2LipClient = Wav2LipClient()

    operator fun invoke(dto: Wav2LipDto, wav2LipIp: String): FileSystemResource {
        logger.info(
            "wav2Lip make start ::: ${
                SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().time)
            }"
        )


        val byteArrayOutputStream: ByteArrayOutputStream =
            grpcClient.determinate(
            dto.audio!!,
            dto.backgroundImg,
            dto.resolution!!,
            wav2LipIp,
            dto.portNum!!.toInt()
        )
        val fileNamePrefix = SimpleDateFormat("HH:mm:ss.SSS").format(Calendar.getInstance().time)
        FileOutputStream("wav2lip/${fileNamePrefix}_wav2_lip_output.mp4").use { outputStream -> byteArrayOutputStream.writeTo(outputStream) }

        logger.info(
            "wav2Lip make end ::: ${
                SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().time)
            }"
        )
        return FileSystemResource(File("wav2lip/${fileNamePrefix}_wav2_lip_output.mp4"))
    }
}