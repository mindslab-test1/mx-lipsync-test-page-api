package maum.ai.lipsync_demo_spring.service

import maum.ai.lipsync_demo_spring.client.LipSyncClient
import maum.ai.lipsync_demo_spring.dto.LipSyncDto
import maum.ai.lipsync_demo_spring.util.LipSyncKey
import maum.ai.lipsync_demo_spring.util.LipSyncStatus
import maum.ai.lipsync_demo_spring.util.LipsyncStatusCode
import org.slf4j.LoggerFactory
import org.springframework.core.io.FileSystemResource
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.text.SimpleDateFormat
import java.util.*

@Service
class LipSyncService{

    private val logger = LoggerFactory.getLogger(LipSyncService::class.java)
    private var grpcClient: LipSyncClient = LipSyncClient()

    private val _processing = 1
    private val _finished = 2
    private val _sleepInterval = 200L

    operator fun invoke(dto: LipSyncDto, lipSyncIp: String): FileSystemResource {
        val uploadResponse = upload(
            lipSyncIp,
            dto.portNum!!.toInt(),
            dto.text!!,
            dto.resolution!!,
            dto.backgroundImg,
            false
        )
        val requestKey: String? = (uploadResponse.payload as LipSyncKey).requestKey
        var prevWaiting = 0
        var lastStatusCode: Int
        logger.info(
            "lipSync make start ::: ${
                SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().time)
            }"
        )

        while (true) {
            val lipSyncCheckResult = statusCheck(requestKey!!)
            val statusCode = (lipSyncCheckResult.payload as LipSyncStatus).statusCode
            val waiting = (lipSyncCheckResult.payload as LipSyncStatus).waiting

            if (waiting >= 0 && waiting != prevWaiting) {
                prevWaiting = waiting
            }

            if (statusCode == _processing) {
                _processing.also { lastStatusCode = it }
                break
            }
            if (statusCode == _finished) {
                _finished.also { lastStatusCode = it }
                break
            }
            Thread.sleep(_sleepInterval)
        }

        while (lastStatusCode != _finished) {
            val lipSyncCheckResult = statusCheck(requestKey)
            lastStatusCode = (lipSyncCheckResult.payload as LipSyncStatus).statusCode
            if (lastStatusCode > _finished)
                throw Exception("LipSync Avatar failed; status code: $lastStatusCode")

            Thread.sleep(_sleepInterval)
        }

        logger.info(
            "lipSync make end ::: ${
                SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().time)
            }"
        )

        download(requestKey)

        val byteArrayOutputStream: ByteArrayOutputStream = download(requestKey)!!

        val fileNamePrefix = SimpleDateFormat("HH:mm:ss.SSS").format(Calendar.getInstance().time)

        FileOutputStream("lipsync/${fileNamePrefix}_lipsync_output.mp4").use { outputStream -> byteArrayOutputStream.writeTo(outputStream) }

        return FileSystemResource(File("lipsync/${fileNamePrefix}_lipsync_output.mp4"))
    }


    fun upload(
        lipSyncIp: String,
        lipSyncPort: Int,
        text: String,
        resolution: String,
        backgroundImg: MultipartFile? = null,
        transparent: Boolean,
    ): LipSyncDto {

        return LipSyncDto(
            message = "UPLOAD SUCCESS",
            payload = grpcClient.upload(
                "$text  .",
                backgroundImg,
                resolution,
                lipSyncIp,
                lipSyncPort
            )
        )

    }

    fun statusCheck (requestKey: String): LipSyncDto {
        val lipSyncStatus: LipSyncStatus = grpcClient.statusCheck(requestKey)
        val statusMessage = when(LipsyncStatusCode.fromOrdinal(lipSyncStatus.statusCode)) {
            LipsyncStatusCode.NOT_YET, LipsyncStatusCode.PROCESSING, LipsyncStatusCode.DONE -> "Success"
            LipsyncStatusCode.DELETED -> "RequestKey is deleted"
            LipsyncStatusCode.WRONG_KEY -> "RequestKey is wrong"
            else -> "Something is wrong"
        }
        return LipSyncDto(message = statusMessage,payload = lipSyncStatus)
    }

    @Throws(Exception::class)
    fun download(
        requestKey: String?
    ): ByteArrayOutputStream? {
        return requestKey?.let { grpcClient.download(it) }
    }

}