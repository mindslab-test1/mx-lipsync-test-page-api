package maum.ai.lipsync_demo_spring.service

import maum.ai.lipsync_demo_spring.dto.ModelCheckDto
import maum.ai.lipsync_demo_spring.util.ModelTypeEnum
import org.slf4j.LoggerFactory
import org.springframework.context.ApplicationContext
import org.springframework.core.io.ClassPathResource
import org.springframework.stereotype.Service
import java.io.BufferedReader
import java.io.File
import java.io.IOException
import java.io.InputStreamReader
import java.nio.file.Files
import java.nio.file.Paths

@Service
class ModelCheckService {
    var logger = LoggerFactory.getLogger(ModelCheckService::class.java)

    operator fun invoke (dto: ModelCheckDto, scriptDir: String) : ModelCheckDto{
         val cmd = arrayOf(scriptDir, dto.portNum.toString())

        try {
            val proc = Runtime.getRuntime().exec(cmd)
            val inputStream = proc.inputStream
            val bufferReader = BufferedReader(InputStreamReader(inputStream))
            var line: String
            if (bufferReader.readLine().also { line = it } != null) {
                logger.info("modelCheck.sh Output ::: $line")
            }
            if(line.length > 1) {
                 when (line[0].toString()){
                    "L" -> {
                        dto.modelType = ModelTypeEnum.LIPSYNC_SERVER
                    }
                    "W" -> {
                        dto.modelType = ModelTypeEnum.WAV2LIP_SERVER
                    }
                    else -> {
                        dto.modelType = ModelTypeEnum.WRONG_MODEL
                    }
                }
            } else {
                dto.modelType = ModelTypeEnum.WRONG_MODEL
            }
            dto.modelName = line
            return dto
        } catch (e: IOException) {
            logger.error("${e.printStackTrace()}")
            dto.modelType = ModelTypeEnum.WRONG_MODEL
            return dto
        }
    }
}