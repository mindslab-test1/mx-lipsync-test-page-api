package maum.ai.lipsync_demo_spring

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication


@SpringBootApplication
class LipsyncDemoSpringApplication

fun main(args: Array<String>) {
	runApplication<LipsyncDemoSpringApplication>(*args)
}
