package maum.ai.lipsync_demo_spring.dto

import org.springframework.web.multipart.MultipartFile
import java.io.Serializable
import javax.validation.constraints.NotBlank

data class Wav2LipDto(
    @field:NotBlank
    var audio: MultipartFile? = null,
    var portNum:String? = null,
    var backgroundImg: MultipartFile? = null,
    var resolution: String? = null,
    var message: String? = null,
    var payload: Serializable? = null
)