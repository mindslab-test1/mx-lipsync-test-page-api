package maum.ai.lipsync_demo_spring.dto

import maum.ai.lipsync_demo_spring.util.ModelTypeEnum
import org.jetbrains.annotations.NotNull

data class ModelCheckDto (

    @field:NotNull
    var portNum: Int? = null,
    var modelName: String? = null,
    var modelType: ModelTypeEnum? = null,

    )