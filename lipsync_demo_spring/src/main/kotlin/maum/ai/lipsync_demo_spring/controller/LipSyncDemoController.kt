package maum.ai.lipsync_demo_spring.controller

import com.fasterxml.jackson.annotation.JsonInclude
import com.google.common.io.ByteStreams.toByteArray
import com.google.common.io.Files.toByteArray
import jdk.jfr.ContentType
import maum.ai.lipsync_demo_spring.dto.LipSyncDto
import maum.ai.lipsync_demo_spring.dto.ModelCheckDto
import maum.ai.lipsync_demo_spring.dto.Wav2LipDto
import maum.ai.lipsync_demo_spring.service.LipSyncService
import maum.ai.lipsync_demo_spring.service.ModelCheckService
import maum.ai.lipsync_demo_spring.service.Wav2LipService
import maum.ai.lipsync_demo_spring.util.InterceptorConfig
import org.apache.tomcat.util.http.fileupload.IOUtils
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.core.io.ByteArrayResource
import org.springframework.core.io.FileSystemResource
import org.springframework.core.io.InputStreamResource
import org.springframework.core.io.Resource
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.io.File
import java.nio.file.Files
import java.nio.file.Paths
import javax.validation.Valid

@RestController
@CrossOrigin
class LipSyncDemoController (
    private val modelCheckService: ModelCheckService,
    private val lipSyncService: LipSyncService,
    private val wav2LipService: Wav2LipService
        ) {
    @Value("\${lipSync.ip}")
    lateinit var lipSyncIp: String

    @Value("\${model.check.script}")
    lateinit var modelCheckScript: String

    var logger = LoggerFactory.getLogger(InterceptorConfig::class.java)

    @PostMapping("/lipsync/model_check/")
    @Valid
    fun checkModelUsePortNumber( dto: ModelCheckDto): ModelCheckDto{
        logger.info("request url ::: /lipsync/model_check/")
        logger.info("request data ::: ${dto.portNum} ")
        val resultModelCheck :ModelCheckDto = modelCheckService(dto, modelCheckScript)
        return resultModelCheck

    }

    @PostMapping("/lipsync/run/text_input/",produces = arrayOf("video/mp4"))
    @ResponseBody
    fun lipSyncModel(dto: LipSyncDto): ResponseEntity<Resource> {
        logger.info("request url ::: /lipsync/run/text_input/")
        logger.info("request data ::: text: ${dto.text} / resolution: ${dto.resolution} / port: ${dto.portNum} / image: ${dto.backgroundImg}")

        val path = Paths.get(lipSyncService(dto, lipSyncIp).file.toURI())
         val resource = InputStreamResource(Files.newInputStream(path))
        return ResponseEntity<Resource>(resource, HttpHeaders(), HttpStatus.OK)
    }


    @PostMapping("/lipsync/run/audio_input/",produces = arrayOf("video/mp4"))
    @Valid
    @ResponseBody
    fun wav2lipModel(dto: Wav2LipDto): ResponseEntity<Resource> {

        logger.info("request url ::: /lipsync/run/audio_input/")
        logger.info("request data ::: audio: ${dto.audio!!.name} / resolution: ${dto.resolution} / port: ${dto.portNum} / image: ${dto.backgroundImg}")

        val path = Paths.get(wav2LipService(dto, lipSyncIp).file.toURI());
         val resource = InputStreamResource(Files.newInputStream(path))
        return ResponseEntity<Resource>(resource, HttpHeaders(), HttpStatus.OK)
    }

}