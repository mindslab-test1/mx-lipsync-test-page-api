package maum.ai.lipsync_demo_spring.util

import java.util.stream.Collectors
import java.util.stream.Stream

enum class ModelTypeEnum {
    LIPSYNC_SERVER,
    WAV2LIP_SERVER,
    WRONG_MODEL
}


enum class LipsyncStatusCode {
    NOT_YET, PROCESSING, DONE, WRONG_KEY, ERROR, DELETED;

    companion object {
        private val LIPSYNC_STATUS_CODE_MAP: Map<Int, LipsyncStatusCode> =
            Stream.of(*values()).collect(Collectors.toMap(
                { e: LipsyncStatusCode -> e.ordinal }
            ) { e: LipsyncStatusCode? -> e }) as Map<Int, LipsyncStatusCode>

        fun fromOrdinal(value: Int): LipsyncStatusCode? {
            return LIPSYNC_STATUS_CODE_MAP[value]
        }
    }
}

