package maum.ai.lipsync_demo_spring.util

import com.fasterxml.jackson.annotation.JsonInclude
import java.io.Serializable


@JsonInclude(JsonInclude.Include.NON_EMPTY)
class LipSyncStatus : Serializable {
    var statusCode = 0
    var message: String? = null
    var waiting = 0

    constructor() {}
    constructor(statusCode: Int, message: String?, waiting: Int) {
        this.statusCode = statusCode
        this.message = message
        this.waiting = waiting
    }

    override fun toString(): String {
        return "LipSyncStatus{" +
                "statusCode=" + statusCode +
                ", message='" + message + '\'' +
                ", waiting=" + waiting +
                '}'
    }
}
