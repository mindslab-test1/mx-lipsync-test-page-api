package maum.ai.lipsync_demo_spring.util

import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import org.springframework.web.servlet.HandlerInterceptor
import org.springframework.web.servlet.ModelAndView
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class InterceptorConfig: HandlerInterceptor {
    val log = LoggerFactory.getLogger(InterceptorConfig::class.java);
    override fun preHandle(request: HttpServletRequest, response: HttpServletResponse, dataObject: Any): Boolean {
        log.info("Request URL  : " + request.requestURI)
        log.info("Request Body: $dataObject")
        return true
    }

    override fun postHandle(
        request: HttpServletRequest,
        response: HttpServletResponse,
        dataObject: Any,
        model: ModelAndView?
    ) {
        log.info(" from PostHandle method.")
    }

    override fun afterCompletion(
        request: HttpServletRequest,
        response: HttpServletResponse,
        dataObject: Any,
        e: Exception?
    ) {
        log.info(e.toString());
        log.info(" from AfterCompletion method - Request Completed!")
    }
}