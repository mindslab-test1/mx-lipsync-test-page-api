package maum.ai.lipsync_demo_spring.util

import com.fasterxml.jackson.annotation.JsonInclude
import java.io.Serializable

@JsonInclude(JsonInclude.Include.NON_EMPTY)
class LipSyncKey : Serializable {
    var requestKey: String? = null

    constructor() {}
    constructor(requestKey: String?) {
        this.requestKey = requestKey
    }

}
